<?php

/*
 * This file is part of the P13 package.
 * 
 * (c) Wagner Sicca <wssicca@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace p13\core\util;

use p13\datetime\Date;
use p13\datetime\DateInterval;
use p13\datetime\DateTime;

/**
 * Classe que realiza conversão de tipos
 *
 * @author Wagner Sicca <wssicca@gmail.com>
 * @namespace p13\core\util
 * @package p13\core\util
 */
class Cast
{

    /**
     * Converte uma string para boolean.
     * @param string $string
     * @return boolean
     */
    public static function toBoolean($string)
    {
        $trueOptions = [1, 's', '1', 'sim', 't', 'y', 'yes'];
        return array_search(mb_convert_case($string, MB_CASE_LOWER), $trueOptions) === false ?
                false :
                true;
    }

    /**
     * Recebe uma string nos formatos DD/MM/YYYY ou YYYY/MM/DD e retorna
     * um objeto DateTime. Se a string informada não estiver em um dos
     * formatos esperados, a função retorna FALSE.
     * 
     * @param string $string
     * @return DateTime|boolean
     */
    public static function toDate($string)
    {
        /**
         * Verifica se a string está no formato DD/MM/YYYY
         */
        if (preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/', $string)) {
            return new Date(implode('-', array_reverse(explode('/', $string))));
        } else
        /**
         * Verifica se a $string é um timestamp Unix
         */
        if (is_int($string)) {
            $data = new Date();
            $data->setTimestamp($string);
            return $data;
        } else
        /**
         * Verifica se a string está no formato YYYY-MM-DD
         */
        if (strtotime($string) !== false) {
            return new Date($string);
        } else {
            return false;
        }
    }

    /**
     * Receve uma string no formado H:i:s e retorna um objeto DateInterval
     * @param string $string
     * @return DateInterval 
     */
    public static function toDateInterval($string)
    {
        if (preg_match('/[0-9]{2}:[0-9]{2}:[0-9]{2}/', $string)) {
            $interval_spec = sprintf('PT%dH%dM%dS', substr($string, 0, 2), substr($string, 3, 2), substr($string, 6, 2));
            return new DateInterval($interval_spec);
        } else if (preg_match('/^[Pp]/', $string)) {
            return DateInterval::createFromDateString($string);
        } else {
            return false;
        }
    }

    /**
     * Recebe uma string nos formatos DD/MM/YYYY ou YYYY/MM/DD e retorna
     * um objeto DateTime. Se a string informada não estiver em um dos
     * formatos esperados, a função retorna FALSE.
     * 
     * @param string $string
     * @return DateTime|boolean
     */
    public static function toDateTime($string)
    {
        /**
         * Verifica se a string está no formato DD/MM/YYYY
         */
        if (preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/', $string)) {
            return new Date(
                    implode('-', array_reverse(explode('/', $string)))
            );
        } else
        /**
         * Verifica se a string está no formato DD/MM/YYYY HH:MI
         */
        if (preg_match('/^\d{1,2}\/\d{1,2}\/\d{4} \d{1,2}:\d{1,2}$/', $string)) {
            $p13 = explode(' ', $string);
            $data = implode('-', array_reverse(explode('/', $p13[0]))) . ' ' . $p13[1];
            return new DateTime($data);
        } else
        /**
         * Verifica se a $string é um timestamp Unix
         */
        if (is_int($string)) {
            $data = new DateTime();
            $data->setTimestamp($string);
            return $data;
        } else
        /**
         * Verifica se a string está no formato YYYY-MM-DD
         */
        if (strtotime($string) !== false) {
            return new DateTime($string);
        } else {
            return false;
        }
    }

}

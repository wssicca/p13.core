<?php

/*
 * This file is part of the P13 package.
 * 
 * (c) Wagner Sicca <wssicca@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace p13\core\util;

use fpdf\FPDF;

/**
 * Description of FPDFUnicode
 *
 * @author Wagner Sicca <wssicca@gmail.com>
 * @namespace p13\core\util
 * @package p13\core\util
 */
class FPDFUnicode extends FPDF
{

    /**
     * 
     * @param int $w 
     * @param int $h 
     * @param string $txt
     * @param mixed $border 
     * @param int $ln
     * @param string $align
     * @param boolean $fill
     * @param string $link
     */
    public function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
    {
        if (mb_detect_encoding($txt) == 'UTF-8') {
            $txt = utf8_decode($txt);
        }
        parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
    }

    /**
     * 
     * @param string $author
     * @param boolean $isUTF8
     */
    public function SetAuthor($author, $isUTF8 = false)
    {
        if (mb_detect_encoding($author) == 'UTF-8') {
            $author = utf8_decode($author);
        }
        parent::SetAuthor($author, $isUTF8);
    }

    /**
     * 
     * @param string $creator
     * @param boolean $isUTF8
     */
    public function SetCreator($creator, $isUTF8 = false)
    {
        if (mb_detect_encoding($creator) == 'UTF-8') {
            $creator = utf8_decode($creator);
        }
        parent::SetCreator($creator, $isUTF8);
    }

    /**
     * 
     * @param string $keywords
     * @param boolean $isUTF8
     */
    public function SetKeywords($keywords, $isUTF8 = false)
    {
        if (mb_detect_encoding($keywords) == 'UTF-8') {
            $keywords = utf8_decode($keywords);
        }
        parent::SetKeywords($keywords, $isUTF8);
    }

    /**
     * 
     * @param string $subject
     * @param boolean $isUTF8
     */
    public function SetSubject($subject, $isUTF8 = false)
    {
        if (mb_detect_encoding($subject) == 'UTF-8') {
            $subject = utf8_decode($subject);
        }
        parent::SetSubject($subject, $isUTF8);
    }

    /**
     * 
     * @param string $title
     * @param boolean $isUTF8
     */
    public function SetTitle($title, $isUTF8 = false)
    {
        if (mb_detect_encoding($title) == 'UTF-8') {
            $title = utf8_decode($title);
        }
        parent::SetTitle($title, $isUTF8);
    }

}

<?php

/*
 * This file is part of the P13 package.
 * 
 * (c) Wagner Sicca <wssicca@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace p13\core\util;

/**
 * Contém métodos úteis para formatação e modificação de strings
 *
 * @author Wagner Sicca <wssicca@gmail.com>
 * @namespace p13\core\util
 * @package p13\core\util
 */
class StringHandler
{

    /**
     * Converte uma string no formato 'CamelCase' para o formato 'camel_case'
     * 
     * @param string $string
     * @return string
     */
    public static function camelCaseParaUnderscore($string)
    {
        $string = preg_replace('/([a-z])([A-Z])/', "$1_$2", $string);
        $string = strtr($string, " ", "_");
        return mb_convert_case($string, MB_CASE_LOWER, "UTF-8");
    }

    /**
     * Converte uma string no formato 'WAGNER DOS SANTOS SICCA'
     * para o formato 'Wagner dos Santos Sicca'
     * @param string $string
     * @return string
     */
    public static function initCap($string)
    {
        $patterns = array(
            0 => '/ De /',
            1 => '/ Do /',
            2 => '/ Dos /',
            3 => '/ Da /',
            4 => '/ Das /',
            5 => '/ E /'
        );
        $replacements = array(
            0 => ' de ',
            1 => ' do ',
            2 => ' dos ',
            3 => ' da ',
            4 => ' das ',
            5 => ' e '
        );
        return preg_replace(
                $patterns, $replacements, mb_convert_case($string, MB_CASE_TITLE, "UTF-8")
        );
    }

    /**
     * Indica se a string está nos formatos DD/MM/YYYY ou YYYY-MM-DD
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isData($string)
    {
        return (boolean) preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/', $string) ||
                (boolean) preg_match('/^\d{4}\-\d{1,2}\-\d{1,2}$/', $string);
    }

    /**
     * Indica se a string está no formato DD/MM/YYYY HH:MI:SS ou YYYY-MM-DD HH:MI
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isDataHora($string)
    {
        return (boolean) preg_match('/^\d{1,2}\/\d{1,2}\/\d{4} \d{1,2}:\d{1,2}$/', $string) ||
                (boolean) preg_match('/^\d{4}\-\d{1,2}\-\d{1,2} \d{1,2}:\d{1,2}$/', $string);
    }

    /**
     * Indica se a string está no formato user@example.com
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isEmail($string)
    {
        return filter_var($string, FILTER_VALIDATE_EMAIL) === false ? false : true;
    }

    /**
     * Indica se a string está no formato user@ifsul.edu.br
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isEmailInstitucional($string)
    {
        return self::isEmail($string) &&
                strpos(mb_convert_case($string, MB_CASE_LOWER), 'ifsul.edu.br') ?
                true :
                false;
    }

    /**
     * Indica se a string é um e-mail hotmail/msn/outlook
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isEmailMicrosoft($string)
    {
        return self::isEmail($string) && (
                strpos(mb_convert_case($string, MB_CASE_LOWER), '@hotmail.com') ||
                strpos(mb_convert_case($string, MB_CASE_LOWER), '@outlook.com') ||
                strpos(mb_convert_case($string, MB_CASE_LOWER), '@msn.com') ||
                strpos(mb_convert_case($string, MB_CASE_LOWER), '@live.com')
                );
    }

    /**
     * Indica se a string informada é uma expressão regular válida
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isExpressaoRegular($string)
    {
        return @preg_match($string, 'Subject') === false ? false : true;
    }

    /**
     * Indica se a string está no formato HH:MI:SS
     * @param string $string
     * @return boolean
     * @static
     */
    public static function isHora($string)
    {
        return (boolean) preg_match('/^\d{1,2}:\d{1,2}$/', $string);
    }

    /**
     * 
     * @param string $email
     * @return string
     */
    public static function mascaraEmail($email)
    {
        $localpart = substr($email, 0, strpos($email, '@'));
        return str_replace(
                substr($localpart, 3, -1), str_repeat('*', strlen($localpart) - 4), $email
        );
    }

    /**
     * Troca caracteres especiais de uma string por caracteres normais
     * 
     * @param string $str
     * @return string
     */
    public static function removeCaracteresEspeciais($str)
    {
        $acentos = array(
            'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
            'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
            'C' => '/&Ccedil;/',
            'c' => '/&ccedil;/',
            'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
            'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
            'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
            'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
            'N' => '/&Ntilde;/',
            'n' => '/&ntilde;/',
            'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
            'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
            'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
            'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
            'Y' => '/&Yacute;/',
            'y' => '/&yacute;|&yuml;/',
            'a.' => '/&ordf;/',
            'o.' => '/&ordm;/'
        );

        return preg_replace($acentos, array_keys($acentos), htmlentities($str, ENT_NOQUOTES, 'UTF-8'));
    }

    /**
     * Recebe um número em segundos e retorna uma string HH:MM
     * @param int $segundos
     * @return string
     */
    public static function segundosParaHoras($segundos)
    {
        $horas = str_pad(round($segundos / 3600), 2, '0', STR_PAD_LEFT);
        $resto = $segundos % 3600;
        $minutos = str_pad(round($resto / 60), 2, '0', STR_PAD_LEFT);

        $horas = floor($segundos / 3600);
        $segundos -= $horas * 3600;
        $minutos = floor($segundos / 60);
        $segundos -= $minutos * 60;

        $horas = str_pad($horas, 2, '0', STR_PAD_LEFT);
        $minutos = str_pad($minutos, 2, '0', STR_PAD_LEFT);

        return "$horas:$minutos";
    }

    /**
     * Converte uma string no formato 'camel_case' para o formato 'CamelCase'
     * Se o parâmetro $capitalise_first_char for TRUE, a primeira letra
     * é colocada como maiúscula
     * 
     * @param string $string
     * @param boolean $capitalise_first_char FALSE
     * @return string
     */
    public static function underscoreParaCamelCase($string, $capitalise_first_char = false)
    {
        if ($capitalise_first_char) {
            $string = ucfirst($string);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $string);
    }

}
